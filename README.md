* https://qiskit.org/documentation/search.html?q=shor&check_keywords=yes&area=default
* https://qiskit.org/documentation/stubs/qiskit.aqua.algorithms.Shor.html?highlight=shor#qiskit.aqua.algorithms.Shor
* https://arxiv.org/abs/quant-ph/0205095
* https://quantum-computing.ibm.com/jupyter/tutorial/fundamentals/4_quantum_circuit_properties.ipynb

* https://quantumcomputing.stackexchange.com/questions/10128/how-many-qubits-does-the-qiskit-implementation-of-shors-algorithm-need-to-facto?rq=1
* https://quantumcomputing.stackexchange.com/questions/8326/is-it-possible-to-run-a-general-implementation-shors-algorithm-on-a-real-ibm-qu
* https://medium.com/@jackkrupansky/ingredients-for-shors-algorithm-for-cracking-strong-encryption-using-a-quantum-computer-847198f47103